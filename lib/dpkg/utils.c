/*
 * libdpkg - Debian packaging suite library routines
 * utils.c - helper functions for dpkg
 *
 * Copyright © 1995, 2008 Ian Jackson <ijackson@chiark.greenend.org.uk>
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <compat.h>

#include <string.h>

#include <dpkg/i18n.h>
#include <dpkg/dpkg.h>

int
fgets_checked(char *buf, size_t bufsz, FILE *f, const char *fn)
{
	int l;

	if (!fgets(buf, bufsz, f)) {
		if (ferror(f))
			ohshite(_("read error in '%.250s'"), fn);
		return -1;
	}
	l = strlen(buf);
	if (l == 0)
		ohshit(_("fgets gave an empty string from '%.250s'"), fn);
	if (buf[--l] != '\n')
		ohshit(_("too-long line or missing newline in '%.250s'"), fn);
	buf[l] = '\0';

	return l;
}

int
fgets_must(char *buf, size_t bufsz, FILE *f, const char *fn)
{
	int l = fgets_checked(buf, bufsz, f, fn);

	if (l < 0)
		ohshit(_("unexpected end of file reading '%.250s'"), fn);

	return l;
}

static const unsigned char mime_base64_rank[256] = {
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255, 62,255,255,255, 63,
   52, 53, 54, 55, 56, 57, 58, 59, 60, 61,255,255,255,  0,255,255,
  255,  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14,
   15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,255,255,255,255,255,
  255, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
   41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
  255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,
};

char *
base64_decode (const char *in, unsigned int *outlen)
{
  const unsigned char *inptr;
  unsigned char *out, *outptr;
  const unsigned char *inend;
  unsigned char c, rank;
  unsigned char last[2];
  unsigned int v;
  int i;
  size_t len;

  len = strlen(in);
  out = malloc((len/4) * 3 + 1);
  inend = (const unsigned char *)in+len;
  outptr = out;

  /* convert 4 base64 bytes to 3 normal bytes */
  v=0;
  i=0;

  last[0] = last[1] = 0;

  /* we use the sign in the state to determine if we got a padding character
     in the previous sequence */
  if (i < 0)
    {
      i = -i;
      last[0] = '=';
    }

  inptr = (const unsigned char *)in;
  while (inptr < inend)
    {
      c = *inptr++;
      rank = mime_base64_rank [c];
      if (rank != 0xff)
        {
          last[1] = last[0];
          last[0] = c;
          v = (v<<6) | rank;
          i++;
          if (i==4)
            {
              *outptr++ = v>>16;
              if (last[1] != '=')
                *outptr++ = v>>8;
              if (last[0] != '=')
                *outptr++ = v;
              i=0;
            }
        }
    }

  *outlen = outptr - out;
  return (char *)out;
}
